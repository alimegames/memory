﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;

public class GameControllerMenu : MonoBehaviour {

    /*
     * TODO: 
     * MUSIC ---> IMPLEMENTATION [x]  MUSIC []
     * LOADER SCREEEN ---> IMPLEMENTATION [] ARTWORK []
     * ABOUT PAGE ---> IMPLEMENTATION [] TEXT[]
     */
    
    //highScoreData.banana
    //highScoreData2.0.melon
    public GameObject highScoreText;
    private HighScoreData currentHS;
    private int tmp_level;
    private float tmp_speed;
    private int tmp_hs_iteration;

    // Use this for initialization
    void Start () {

        tmp_level = PlayerPrefs.GetInt("level");
        tmp_speed = PlayerPrefs.GetFloat("speed");
        tmp_hs_iteration = PlayerPrefs.GetInt("hs");
        PlayerPrefs.DeleteAll();

        GetSavedHighScore();
        LoadHighScore();
    }

    private void GetSavedHighScore()
    {
        if (File.Exists(Application.persistentDataPath + "/highScoreData2.0.melon"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/highScoreData2.0.melon", FileMode.Open);

            currentHS = (HighScoreData)bf.Deserialize(file);
            file.Close();            
        }
        else
        {
            currentHS = new HighScoreData();
            currentHS.level = 0;
            currentHS.speed = 0;
        }

        if (tmp_hs_iteration > currentHS.hs_iteration)
        {
            SaveGame();
        }

    }
    
    
    private void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/highScoreData2.0.melon");

        HighScoreData hs = new HighScoreData();
        hs.level = tmp_level;
        hs.speed = tmp_speed;
        hs.hs_iteration = tmp_hs_iteration;

        bf.Serialize(file, hs);
        file.Close();
    }
    
    private void LoadHighScore()
    {
        if(File.Exists(Application.persistentDataPath + "/highScoreData2.0.melon"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/highScoreData2.0.melon", FileMode.Open);

            HighScoreData hs = (HighScoreData) bf.Deserialize(file);
            
            file.Close();
            highScoreText.GetComponent<Text>().text = "Max Level: " + hs.level + "\nMax Speed: " + hs.speed;
        }
        else
        {
            highScoreText.GetComponent<Text>().text = "";
        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void StartGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    [Serializable]
    class HighScoreData
    {
        public int level;
        public float speed;
        public int hs_iteration;
    }
}
