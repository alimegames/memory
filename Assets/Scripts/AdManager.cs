﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using admob;

public class AdManager : MonoBehaviour {

	public static AdManager Instance { set; get; }

    private void Start()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
        
#if UNITY_EDITOR
#elif UNITY_ANDROID
        Admob.Instance().initAdmob("ca-app-pub-8848128488519839/9951708261", "ca-app-pub-8848128488519839/8704616339");
        Admob.Instance().setTesting(true);
        Admob.Instance().loadInterstitial();
#endif
    }

    public void ShowBanner()
    {
#if UNITY_EDITOR
#elif UNITY_ANDROID
        Admob.Instance().showBannerRelative(AdSize.Banner, AdPosition.BOTTOM_CENTER, 0);
#endif
    }

    public void ShowVideo()
    {
#if UNITY_EDITOR
#elif UNITY_ANDROID
        if (Admob.Instance().isInterstitialReady())
        {
            Admob.Instance().showInterstitial();
        }
#endif
    }
}
