﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameController : MonoBehaviour {

    //TODO: bolas desaparecerem antes do tempo

    //LEVELS CONFIGURATIONS
    private Dictionary<int, Level> levels;
    //-LEVELS CONFIGURATIONS

    //DUMMIES
    public GameObject DUMMY_RIGHT;
    public GameObject DUMMY_WRONG;
    //-DUMMIES

    //LIFES
    public GameObject lifes;
    public Sprite oneLife;
    public Sprite twoLifes;
    public Sprite threeLifes;
    //-LIFES

    //BALL SPRITES
    public Sprite RedSprite;
        public Sprite BlueSprite;
        public Sprite GreenSprite;
        public Sprite YellowSprite;

        private Dictionary<string, Sprite> spriteList;
        private string[] colorList;
    //-BALL SPRITES

    //GAME OBJECTS
        public static GameController instance;
        public GameObject levelsObject;
        private GameObject currentLevelGameObject;
        public GameObject currentlLevelText;
        public GameObject saveGame;
    //-GAME OBJECTS

    //GAME VARIABLES
        private bool waiting_user_input = false;
        public bool gameOver = false;
        private int numberOfLifes = 3;
        private Level currentLevelObject;
        private int currentLevel = 1;
        private int currentLevelIteration = 1;
        private int overallHighScoreIteration = 1;
        private float currentLevelIterationTimer;
        private string[] currentLevelGeneratedColorList;
        private string[] currentLevelInputColorList;
        private int currentInputIndex;
        private Transform[] currentLevelChildList;
        private float INCREASEVALUE = 1.322f;
        private float OVERHEAD = 0.1f;
    //-GAME VARIABLES

    //GAME PROGRESS
    private
    //-GAME PROGRESS
    
    System.Random generator;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {

        AdManager.Instance.ShowBanner();

        //initialize sprite list
        spriteList = new Dictionary<string, Sprite>();
        spriteList.Add("Red", RedSprite);
        spriteList.Add("Blue", BlueSprite);
        spriteList.Add("Green", GreenSprite);
        spriteList.Add("Yellow", YellowSprite);

        //initialize color list
        colorList = new string[spriteList.Count];
        colorList[0] = "Red";
        colorList[1] = "Blue";
        colorList[2] = "Green";
        colorList[3] = "Yellow";

        //levels configurations
        levels = new Dictionary<int, Level>();
        CreateLevels();

        //others
        generator = new System.Random();

        currentlLevelText.GetComponent<Text>().text = "Level: " + currentLevel + "\nSpeed: " + currentLevelIterationTimer;
        UpdateLevelObject();

        StartCoroutine(StartGame());
        
    }
    
    public void ButtonPressed(string buttonColor)
    {
        /*
        if (currentLevelGameObject != null)
        {
            foreach (Transform child in currentLevelGameObject.transform)
            {
                Sprite result;
                spriteList.TryGetValue(buttonColor, out result);
                child.GetComponent<SpriteRenderer>().sprite = result;
            }
        }
        */

        if (waiting_user_input && !gameOver)
        {
            
            currentLevelInputColorList[currentInputIndex] = buttonColor;
            PresentInputBall();
            currentInputIndex++;

            if (currentInputIndex >= currentLevelInputColorList.Length)
            {
                waiting_user_input = false;
                //block user input and assert
                StartCoroutine(AssertCorrectness());
                
            }
        }
    }

    private void PresentInputBall()
    {
        Sprite result;
        spriteList.TryGetValue(currentLevelInputColorList[currentInputIndex], out result);

        currentLevelChildList[currentInputIndex].GetComponent<SpriteRenderer>().sprite = result;
        currentLevelChildList[currentInputIndex].GetComponent<SpriteRenderer>().enabled = true;
    }

    private IEnumerator AssertCorrectness()
    {
        if (currentLevelGeneratedColorList.SequenceEqual(currentLevelInputColorList))
        {
            currentLevelIteration++;
            overallHighScoreIteration++;
            yield return new WaitForSeconds(0.5f);
            
        }
        else
        {
            Handheld.Vibrate();
            yield return new WaitForSeconds(0.5f);
            //take life
            
            if (numberOfLifes == 3)
                removeLife(twoLifes);
            else if (numberOfLifes == 2)
                removeLife(oneLife);
            else
            {
                gameOver = true;
                SaveGame();
                SceneManager.LoadScene("Menu");
            }
                
            numberOfLifes--;
            
        }
                
        if (!gameOver)
        {
            //LET'S GO NEXT BABY
            ClearBalls();
            yield return new WaitForSeconds(0.5f);
            SetupLevelIterations();
            InitializeBalls();
        }
            

    }

    private void SaveGame()
    {
        PlayerPrefs.SetInt("level", currentLevel);
        PlayerPrefs.SetFloat("speed", currentLevelIterationTimer);
        PlayerPrefs.SetInt("hs", overallHighScoreIteration);
    }
        

    private void removeLife(Sprite currentLife)
    {
        lifes.GetComponent<SpriteRenderer>().sprite = currentLife;
    }

    public void IncreaseLevel()
    {
        currentLevel++;
        ClearBalls();
        //InitializeBalls();
        currentlLevelText.GetComponent<Text>().text = "Level: " + currentLevel;
        UpdateLevelObject();
    }

    public IEnumerator StartGame()
    {
        yield return new WaitForSeconds(1f);
        gameOver = false;
        currentLevelIteration = 1;
        //currentLevel = 1;
        SetupLevelIterations();
        InitializeBalls();
    }

    private void InitializeBalls()
    {
        for (int i = 0; i < levelsObject.transform.childCount; i++)
        {
            if (levelsObject.transform.GetChild(i).name.Equals("Level" + currentLevel))
            {
                currentLevelGameObject = levelsObject.transform.GetChild(i).gameObject;

                //initialize level array
                currentLevelGeneratedColorList = new string[currentLevelGameObject.transform.childCount];
                currentLevelInputColorList = new string[currentLevelGameObject.transform.childCount];
                currentInputIndex = 0;

                currentLevelChildList = new Transform[currentLevelGameObject.transform.childCount];
                int iteratorAuxiliar = 0;
                foreach (Transform child in currentLevelGameObject.transform)
                {
                    currentLevelChildList[iteratorAuxiliar] = child;
                    
                    child.GetComponent<SpriteRenderer>().sprite = GenerateColor(iteratorAuxiliar);

                    iteratorAuxiliar++;
                }

                StartCoroutine(EnableChilds());
                break;
            }
            else
            {
                //useless????
                currentLevelGameObject = levelsObject.transform.GetChild(i).gameObject;

                foreach (Transform child in currentLevelGameObject.transform)
                {
                    child.GetComponent<SpriteRenderer>().enabled = false;
                }
            }
        }
        
    }
    
    private void ClearBalls()
    {
        for (int i = 0; i < levelsObject.transform.childCount; i++)
        {
            foreach (Transform child in currentLevelGameObject.transform)
            {
                child.GetComponent<SpriteRenderer>().enabled = false;
            }            
        }
    }

    private IEnumerator EnableChilds()
    {
        int[] arrayAuxiliarl = CreateArray(currentLevelChildList.Length);
        arrayAuxiliarl = ShuffleArray(arrayAuxiliarl);
        for (int i = 0; i < arrayAuxiliarl.Length; i++)
        {
            currentLevelChildList[arrayAuxiliarl[i]].GetComponent<SpriteRenderer>().enabled = true;
                        
            yield return new WaitForSeconds(currentLevelIterationTimer);

            if (i == arrayAuxiliarl.Length-1)
                ClearBalls();
            
        }

        waiting_user_input = true;

    }

    private int[] CreateArray(int size)
    {
        int[] array = new int[size];
        for (int i = 0; i < size; i++)
        {
            array[i] = i;
        }

        return array;
    }

    int[] ShuffleArray(int[] array)
    {
        for (int i = array.Length; i > 0; i--)
        {
            int j = generator.Next(i);
            int k = array[j];
            array[j] = array[i - 1];
            array[i - 1] = k;
        }
        return array;
    }


    /*
     * GENERATE COLOR RULES:
     * 1- the first ball can have any color.
     * 2- any balls that are not the first must obly the following rules:
     *  a) the ball can only have a color if that color hasn't appeared more than once yet and if that color isn't the same as the previous.
     *  
     * TL;DR: a color can only appear twice (at max) and never can two colors appear together
    */
    private Sprite GenerateColor(int i)
    {
        string[] availableColors = ColorsThatCanAppear(i);
        int numberGenerated = generator.Next(0, availableColors.Length);

        Sprite result;
        spriteList.TryGetValue(availableColors[numberGenerated], out result);

        currentLevelGeneratedColorList[i] = availableColors[numberGenerated];

        return result;
    }

    private string[] ColorsThatCanAppear(int i)
    {
        if (i == 0)
            return colorList;

        List<String> unavailableColors = new List<String>();

        //last color
        string lastColor = currentLevelGeneratedColorList[i - 1];
        unavailableColors.Add(lastColor);

        //colors that appeared more than once
        List<String> currentLevelGeneratedColorList_list = currentLevelGeneratedColorList.ToList();

        var duplicates = currentLevelGeneratedColorList_list
            .GroupBy(item => item).SelectMany(grp => grp.Skip(1).Take(1)).Where(e => e != null);

        foreach (String color in duplicates)
        {
            unavailableColors.Add(color);
        }

        List<String> availableColors = colorList.ToList();
        availableColors.RemoveAll(r => unavailableColors.Any(a => a == r));

        return availableColors.ToArray();
    }

    public void CreateLevels()
    {
        //level 1
        string level1_id = "Level1 - the easy mode";
        int lelvel1_number_of_balls = 3;
        float[] level1_iterations = { 0.6f, 0.55f };
        int[] lelvel1_iteration_transition = { 2, 4};
        Level level1 = new Level(level1_id, lelvel1_number_of_balls, level1_iterations, lelvel1_iteration_transition);

        levels.Add(1, level1);

        //level 2
        string level2_id = "Level2 - i got this";
        int lelvel2_number_of_balls = 4;
        float[] level2_iterations = { 0.55f, 0.50f, 0.45f };
        int[] lelvel2_iteration_transition = { 2, 4, 6 };
        Level level2 = new Level(level2_id, lelvel2_number_of_balls, level2_iterations, lelvel2_iteration_transition);

        levels.Add(2, level2);

        //level 3
        string level3_id = "Level3 - not a chance";
        int lelvel3_number_of_balls = 5;
        //float[] level3_iterations = { 0.5f, 0.3f };
        //int[] lelvel3_iteration_transition = { 2, 4 };
        Level level3 = new Level(level3_id, lelvel3_number_of_balls, null, null);

        levels.Add(3, level3);
    }

    private void UpdateLevelObject()
    {
        levels.TryGetValue(currentLevel, out currentLevelObject);
    }

    private void SetupLevelIterations()
    {
        if (currentLevelObject.level_iteration_transition == null)
        {
            currentLevelIterationTimer = CalculateInfinteLevelSpeed();
        }
        else
        {
            if (currentLevelIteration > currentLevelObject.level_iteration_transition[currentLevelObject.level_iteration_transition.Length - 1])
            {
                
                currentLevel++;
                currentLevelIteration = 1;
                UpdateLevelObject();
                
                //gameOver = true;
                //currentlLevelText.GetComponent<Text>().text = "LEVEL ENDED!";
                //return;
            }
            if (currentLevelObject.level_iteration_transition == null)
            {
                currentLevelIterationTimer = CalculateInfinteLevelSpeed();
            }
            else
            {
                for (int i = 0; i < currentLevelObject.level_iteration_transition.Length; i++)
                {
                    if (currentLevelIteration <= currentLevelObject.level_iteration_transition[i])
                    {
                        //Debug.Log(currentLevelObject.level_iterations[i]);
                        currentLevelIterationTimer = currentLevelObject.level_iterations[i];
                        break;
                    }
                }
            }
            
        }
        
        currentlLevelText.GetComponent<Text>().text = "Level: " + currentLevel + "\nSpeed: " + currentLevelIterationTimer;
    }

    private float CalculateInfinteLevelSpeed()
    {
        float iteration = (currentLevelIteration / 10.0f) + INCREASEVALUE - OVERHEAD;

        return (float) Math.Round(Math.Pow(0.5, iteration) + OVERHEAD,3);
    }

    //LEVELS CONFIGURATION CLASS
    private class Level
    {
        public string level_id;
        public int level_number_of_balls;
        public float[] level_iterations;
        public int[] level_iteration_transition;

        public Level(string id, int number_of_balls, float[] iterations, int[] iteration_transition)
        {
            level_id = id;
            level_number_of_balls = number_of_balls;
            level_iterations = iterations;
            level_iteration_transition = iteration_transition;
        }
    }
    
}
